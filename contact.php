﻿<?php
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 1000");
	header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
	
	if (!empty($_POST["email"]) && !empty($_POST["message"]) && !empty($_POST["securite"])) {
		if (filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) && intval($_POST["securite"]) === 23) {
			$to = "ez@ladigitale.dev";					 
			$subject = "Nouveau message La Digitale";
			$message = "<p>Adresse &eacute;lectronique : " . $_POST["email"] . "</p><p>Message : " . $_POST["message"] . "</p>";
			$headers = "From:" . $_POST["email"] . "\r\n";
			$headers .= "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			mail($to, $subject, $message, $headers);
			echo "message_envoye";
		} else if (intval($_POST["securite"]) !== 23) {
			echo "erreur_captcha";
		} else {
			echo "erreur";
		}
		exit();
	} else {
		header("Location: /");
	}
?>
