# ladigitale.dev

Le site de La Digitale (https://ladigitale.dev), publié sous licence GNU AGPLv3. Sauf les fontes Elsie Swash Cap, Material Icons, Medula One (Apache License Version 2.0) et la fonte HKGrotesk (Sil Open Font Licence 1.1)
